;(function($, doc, win) {
    "use strict";
    
    
    function ColorPicker(el, opts) {
    
        this.$el = $(el);
        this.self = this;
        
        this.width = this.$el.outerWidth();
        this.height = this.$el.outerHeight();
        
        this.defaults = {
            
            size : 'medium',
            defaultColor : '#ff66ff'
            
        }
        
        var meta = this.$el.data('colorpicker-opts');
        this.$el.data('colorpicker', this);
        this.opts = $.extend(this.defaults, opts, meta);
        
        
        this.colorValue = this.opts.defaultColor;
        
        this.colorPreviewWrapper = null;
        this.colorPickerPanel = null;
        
        
        this.mainCanvas = {
            mainCanvas : null,
            mainCanvasContext : null,
            mainCanvasPointer : null,
            wrapper : null
        };
        
        this.hueCanvas = {
            hueCanvasElement : null,
            hueCanvasContext : null,
            hueCanvasPointer : null,
            wrapper : null
        };
        
        
        this.panelShown = false;
        
        this.dragging = false;
        this.draggingElement = null;
        
        this.init();
    }
    
    /**
     *  MAIN
     */
    
    ColorPicker.prototype.init = function() {
        
        this.createColorPreviewWrapper();
        this.createColorPickerWrapper();
        
        this.createMainCanvas();
        
        
        this.createHueCanvas();
        
        
        this.setSelectedColorAndHue(this.opts.defaultColor);
        
        this.defineEvents();
        
        this.$el.css('display', 'none');
        
        this.colorPickerPanel.css('visibility', 'visible');
        this.colorPickerPanel.css('display', 'none');
    }
    
     /**
     *  BASE ELEMENT CREATION
     */
    
    ColorPicker.prototype.createColorPreviewWrapper = function() {
        
        var wrapper = $('<div/>');
        var colorElement = $('<div/>');
        
        wrapper.css('position',  'absolute');
        wrapper.data('name','colorPreviewWrapperWrapper');
        wrapper.css('cursor', 'pointer');
        
        colorElement.height(this.height);
        colorElement.width(this.width);
        
        wrapper.append(colorElement);
        this.$el.parent().append(wrapper);
        
        wrapper.offset({
            top: this.$el.offset().top, 
            left: this.$el.offset().left
        });
        
        this.colorPreviewWrapper = colorElement;
    }
    
    /**
     *  MAIN PANEL VIEW CREATOR
     */
    
    ColorPicker.prototype.createColorPickerWrapper = function() {
        
        var wrapper = $('<div/>');
        
        wrapper.css('position', 'absolute');
        wrapper.data('name', 'mainSelectorWrapper');
        
        wrapper.height("200");
        wrapper.width("250");
        
        wrapper.css('border', '1px solid grey');
        wrapper.css('background-color', '#fff');
        wrapper.css('visibility', 'hidden');
        
        
        $('body').append(wrapper);
        
        this.colorPickerPanel = wrapper;
    }
    
    
    
    /**
     *  MAIN PANEL VIEW CONTROLLER CREATOR
     */
    
    ColorPicker.prototype.createMainCanvas = function() {
        
        var mainCanvasWrapper = $('<div/>');
        mainCanvasWrapper.css('display', 'inline-flex');
        
        var canvas = $('<canvas/>');
        
        canvas.css('border', '1px solid black');
        canvas.css('margin-top', '2vh');
        canvas.css('margin-left', '1vw');
        
        canvas.attr('height', Math.floor(this.colorPickerPanel.height()*0.8));
        canvas.attr('width', Math.floor(this.colorPickerPanel.width()*0.67));
        
        this.mainCanvas.mainCanvas = canvas;
        this.mainCanvas.mainCanvasContext = $(canvas).get(0).getContext("2d");
        
        
        mainCanvasWrapper.append(canvas);
        this.mainCanvas.wrapper = mainCanvasWrapper;
        this.colorPickerPanel.append(mainCanvasWrapper);
        this.createMainCanvasPointer();
    }
    
    ColorPicker.prototype.createMainCanvasPointer = function() {
        
        var mainCanvasPointer = $('<div/>');
        
        mainCanvasPointer.css('border', '1px solid black');
        mainCanvasPointer.css('border-radius', '100%');
        
        mainCanvasPointer.css('height', '6px');
        mainCanvasPointer.css('width', '6px');
        
        mainCanvasPointer.css('background-color', '#fff');
        mainCanvasPointer.css('position', 'absolute');
        
        this.mainCanvas.wrapper.append(mainCanvasPointer);
        
        mainCanvasPointer.offset({
            top : this.mainCanvas.mainCanvas.offset().top,
            left : this.mainCanvas.mainCanvas.offset().left + 1.0
        });
        
        this.mainCanvas.mainCanvasPointer = mainCanvasPointer;
    }
   
   
    ColorPicker.prototype.createHueCanvas = function() {
        
        var hueCanvasWrapper = $('<div/>');
        hueCanvasWrapper.css('display', 'inline-flex');
        
        var canvas = $('<canvas/>');
        
        canvas.css('border', '1px solid black');
        canvas.css('margin-left', '0.5vw');
        
        canvas.attr('height', Math.floor(this.colorPickerPanel.height()*0.8));
        canvas.attr('width', Math.floor(this.colorPickerPanel.width()*0.1));
        
        let context = $(canvas).get(0).getContext("2d");
        
        this.hueCanvas.hueCanvasElement = canvas;
        this.hueCanvas.hueCanvasContext = context;
        
        let gradient = context.createLinearGradient(0, 0, 0, context.canvas.height);
        for(var i = 0; i < 360; i ++) {
            
            gradient.addColorStop( (i / 360),  'hsl('+(360-i)+',100%,50%)');
            
        }
        context.fillStyle = gradient
        context.fillRect(0, 0, context.canvas.width, context.canvas.height);
        
        hueCanvasWrapper.append(canvas);
        this.hueCanvas.wrapper = hueCanvasWrapper;
        this.colorPickerPanel.append(hueCanvasWrapper);
        this.createHueCanvasSlider();
    }
    
    ColorPicker.prototype.createHueCanvasSlider = function() {
        
        var hueCanvasPointer = $('<div/>');
        
        hueCanvasPointer.css('border', '1px solid black');
        
        hueCanvasPointer.css('height', '3px');
        hueCanvasPointer.css('background-color', '#fff');
        hueCanvasPointer.css('width', this.hueCanvas.hueCanvasContext.canvas.width);
        hueCanvasPointer.css('position', 'absolute');
        
        this.hueCanvas.wrapper.append(hueCanvasPointer);
        
        hueCanvasPointer.offset({
            top : this.hueCanvas.hueCanvasElement.offset().top,
            left : this.hueCanvas.hueCanvasElement.offset().left + 1.0
        });
        
        this.hueCanvas.hueCanvasPointer = hueCanvasPointer;
    }
    
    
    
    
    ColorPicker.prototype.setMainSliderPosition = function(offset) {
        
        if(offset < 0 || offset > this.hueCanvas.hueCanvasContext.canvas.height)
            return false;
        
        this.hueCanvas.hueCanvasPointer.offset({
            top : offset + this.hueCanvas.hueCanvasElement.offset().top
        });
    }
    
    ColorPicker.prototype.setMainCanvasPointerPosition = function(offsetX, offsetY) {
        
        var mainCanvasPointerCenterHeight = this.mainCanvas.mainCanvasPointer.height()/2;
        var mainCanvasPointerCenterWidth = this.mainCanvas.mainCanvasPointer.width()/2;
        
        var canvasHeight = this.mainCanvas.mainCanvasContext.canvas.height;
        var canvasWidth = this.mainCanvas.mainCanvasContext.canvas.width;
        
        var canvasTopOffset = this.mainCanvas.mainCanvas.offset().top;
        var canvasLeftOffset = this.mainCanvas.mainCanvas.offset().left;
        
        var finalY, finalX;
        
        if(offsetY + mainCanvasPointerCenterHeight < 0)
            finalY = canvasTopOffset - mainCanvasPointerCenterHeight;
        else if(offsetY + mainCanvasPointerCenterHeight > canvasHeight)
            finalY = canvasTopOffset + canvasHeight - mainCanvasPointerCenterHeight;
        else
            finalY = offsetY + canvasTopOffset;
        
        if(offsetX + mainCanvasPointerCenterWidth < 0)
            finalX = canvasLeftOffset - mainCanvasPointerCenterWidth;
        else if(offsetX + mainCanvasPointerCenterWidth > canvasWidth)
            finalX = canvasLeftOffset + canvasWidth - mainCanvasPointerCenterWidth;
        else
            finalX = offsetX + canvasLeftOffset;
        
        
        this.mainCanvas.mainCanvasPointer.offset({
            
            top : finalY,
            
            left : finalX
            
        });
        
        
        this.setFinalColor(this.getMainPointerCurrentColor());

    }
    
    
    /**
     *  COLOR STUFF
     */
    
    ColorPicker.prototype.setColor = function(color) {
        
        if(this.panelShown == false) {
            this.colorPickerPanel.css('visibility', 'hidden');
            this.colorPickerPanel.css('display', 'block');
        }
        
        this.setSelectedColorAndHue(color);
        
    }
    
    ColorPicker.prototype.setSelectedColorAndHue = function(color) {
        
        this.setMainHueSliderPositionFromColor(color);
        this.setMainPointerPositionFromColor(color);
        this.setFinalColor(color);   
        
    }
    
    
    ColorPicker.prototype.setMainHueSliderPositionFromColor = function(color) {
        
        this.setMainSliderPosition(this.findSliderCoordinatesByColor(color));
        let hue = this.obtainColorHueFromHex(color);
        this.fillCanvas(this.mainCanvas.mainCanvasContext, hue);
        /*this.setFinalColor(this.getMainPointerCurrentColor());*/
        
    }
    
    ColorPicker.prototype.setMainPointerPositionFromColor = function(color) {
        
        let pointer = this.mainCanvas.mainCanvasPointer;
        var offset = this.findMainPointerCoordinateByColor(color);
        this.setMainCanvasPointerPosition(offset.x - pointer.width()/2, offset.y - pointer.height()/2);
        
    }
    
    ColorPicker.prototype.setMainHueFromSliderPosition = function(coordinate) {
        
        let canvasPixelsData = this.hueCanvas.hueCanvasContext.getImageData(0, coordinate, 1, 1);
        let hexColor = rgbToHex(canvasPixelsData.data[0], canvasPixelsData.data[1], canvasPixelsData.data[2]);
        let hue = this.obtainColorHueFromHex(hexColor);
        this.fillCanvas(this.mainCanvas.mainCanvasContext, hue);
        this.setFinalColor(this.getMainPointerCurrentColor());
        
    }
    
    ColorPicker.prototype.setFinalColor = function(color) {
        
        if(this.panelShown == false) {
            this.colorPickerPanel.css('display', 'none');
            this.colorPickerPanel.css('visibility', 'visible');
        }
        
        this.colorPreviewWrapper.css('background-color', color);
        this.colorValue = color;
        this.$el.val(color);
        
    }
    
    
    ColorPicker.prototype.findMainPointerCoordinateByColor = function(color) {
        
        let canvasPixelsData = this.mainCanvas.mainCanvasContext.getImageData(0, 0, this.mainCanvas.mainCanvasContext.canvas.width, this.mainCanvas.mainCanvasContext.canvas.height).data;
        
        let rgbColor = hexToRgb(color);
        let transformedRgbColor = (rgbColor.r)+(rgbColor.g*255)+(rgbColor.b*255*255);
        
        var finalSearchArray = [];
        
        for(var i = 0; i < canvasPixelsData.length; i += 4) {
            
            finalSearchArray.push((canvasPixelsData[i]) + (canvasPixelsData[i+1] * 255) + (canvasPixelsData[i+2] * 255 * 255));
        }

        var closestColor = finalSearchArray.reduce(function (prev, curr) {
            return (Math.abs(curr - transformedRgbColor) < Math.abs(prev - transformedRgbColor) ? curr : prev);
        });
            
        var arrayPos = finalSearchArray.indexOf(closestColor);
        
        var x = Math.floor(arrayPos % this.mainCanvas.mainCanvasContext.canvas.width);
        var y = Math.floor(arrayPos / this.mainCanvas.mainCanvasContext.canvas.width);

        return {x : x, y : y};
    }
    
    ColorPicker.prototype.getMainPointerCurrentColor = function() {
        
        let pointer = this.mainCanvas.mainCanvasPointer;
        
        let offset = this.mainCanvas.mainCanvasPointer.offset();
        let parentOffset = this.mainCanvas.mainCanvas.offset();
        
        let finalOffsetX, finalOffsetY;
        let calculationX = Math.floor(offset.left - parentOffset.left + (pointer.width()/2));
        let calculationY = Math.floor(offset.top - parentOffset.top + (pointer.height()/2));
        
        if(calculationX == this.mainCanvas.mainCanvasContext.canvas.width)
            finalOffsetX = this.mainCanvas.mainCanvasContext.canvas.width - 1;
        else
            finalOffsetX = calculationX;
        
        if(calculationY == this.mainCanvas.mainCanvasContext.canvas.height)
            finalOffsetY = this.mainCanvas.mainCanvasContext.canvas.height - 1;
        else
            finalOffsetY = calculationY;
        
        let canvasPixelsData = this.mainCanvas.mainCanvasContext.getImageData( finalOffsetX , finalOffsetY , 1, 1);
        
        return rgbToHex(canvasPixelsData.data[0], canvasPixelsData.data[1], canvasPixelsData.data[2]);
    }
    
    ColorPicker.prototype.fillCanvas = function(canvasContext, currentHue) {
        
        var grH = canvasContext.createLinearGradient(0, 0, canvasContext.canvas.width, 0);
        grH.addColorStop(0, '#ffffff');
        grH.addColorStop(1, 'hsl('+currentHue+', 100%, 50%)');

        canvasContext.fillStyle = grH;
        canvasContext.fillRect(0,0, canvasContext.canvas.width, canvasContext.canvas.height);

        var grV = canvasContext.createLinearGradient(0, 0, 0, canvasContext.canvas.height);
        grV.addColorStop(0, 'rgba(0,0,0,0)');
        grV.addColorStop(1,  '#000000');

        canvasContext.fillStyle = grV;
        canvasContext.fillRect(0, 0, canvasContext.canvas.width, canvasContext.canvas.height);
    }
    
    
    
    ColorPicker.prototype.findSliderCoordinatesByColor = function(color) {
        
        let hue = this.obtainColorHueFromHex(color);
        let canvasPixelsData = this.hueCanvas.hueCanvasContext.getImageData(0, 0, 1, this.hueCanvas.hueCanvasContext.canvas.height);
        
        let pixelsArray = [];
        for(var i = 0; i < canvasPixelsData.data.length; i += 4) {
            pixelsArray.push(rgbToHsl(canvasPixelsData.data[i], canvasPixelsData.data[i+1], canvasPixelsData.data[i+2])[0]*360);
        }
        
        var closestColor = pixelsArray.reduce(function (prev, curr) {
            return (Math.abs(curr - hue) < Math.abs(prev - hue) ? curr : prev);
        });
        
        return pixelsArray.indexOf(closestColor);
    }
    
    
    ColorPicker.prototype.obtainColorHueFromHex = function(color) {
        
        let rgbColor = hexToRgb(color);
        return rgbToHsl(rgbColor.r, rgbColor.g, rgbColor.b)[0]*360;
        
    }
    
    
    /**
     *  MAIN PANEL VIEW CONTROLLER
     */
    
    ColorPicker.prototype.toggleMainSelectorPanel = function() {
        
        if(this.panelShown == false) {
            
            this.colorPickerPanel.css('display', 'block');
            
            this.colorPickerPanel.offset({
                top : this.colorPreviewWrapper.offset().top + (this.colorPreviewWrapper.height()/2),
                left : this.colorPreviewWrapper.offset().left + (this.colorPreviewWrapper.width()/2)
            });
            
            this.panelShown = true;
            
        }
        else {
            
            this.colorPickerPanel.css('display', 'none');
            this.panelShown = false;
            
        }
        
    }
    
    
    ColorPicker.prototype.defineEvents = function() {
        
        let self = this.self;
        
        self.colorPreviewWrapper.click(function(event) {
           
            self.toggleMainSelectorPanel();
            
            event.preventDefault();
            event.stopPropagation();
            
        });
        
        self.colorPickerPanel.click(function(event) { 
            
            event.preventDefault();
            event.stopPropagation();
            
        });
        
        self.hueCanvas.wrapper.mousedown(function(event) {
            
            self.dragging = true;
            self.draggingElement = self.hueCanvas.hueCanvasPointer;
            
            event.preventDefault();
            event.stopPropagation();
            
        });
        
        self.mainCanvas.wrapper.mousedown(function(event) {
            
            self.dragging = true;
            self.draggingElement = self.mainCanvas.mainCanvasPointer;
            
            event.preventDefault();
            event.stopPropagation();
            
        });
        
        
        
        
        
        
        var globalMouseMove = $(document).mousemove(function(event) {
           
            if(self.dragging == true) {
                
                if(self.draggingElement == self.hueCanvas.hueCanvasPointer) {
                    
                    self.setMainSliderPosition(event.pageY - self.hueCanvas.hueCanvasElement.offset().top);
                    self.setMainHueFromSliderPosition(event.pageY - self.hueCanvas.hueCanvasElement.offset().top);
                    
                    event.preventDefault();
                    event.stopPropagation();
                    
                }
                else if(self.draggingElement == self.mainCanvas.mainCanvasPointer) {
                    
                    self.setMainCanvasPointerPosition(event.pageX - self.mainCanvas.mainCanvas.offset().left - self.mainCanvas.mainCanvasPointer.outerWidth()/2, event.pageY - self.mainCanvas.mainCanvas.offset().top - self.mainCanvas.mainCanvasPointer.outerHeight()/2);
                    
                    event.preventDefault();
                    event.stopPropagation();
                    
                }
                
            }
            
        });
        
        var globalMouseUp = $(document).mouseup(function(event) {
           
            if(self.dragging == true) {
                self.dragging = false;
                self.draggingElement = null;
                
                event.stopPropagation();
                event.preventDefault();
            }
            
        });
        
        var globalClick = $(document).click(function(event) {
           
            if(self.panelShown == true) {
                self.toggleMainSelectorPanel();
            }
                
            event.stopPropagation();
            event.preventDefault();
            
        });
        

        
    }
    
    
    $.fn.colorPicker = function(opts) {
        return this.each(function() {
            new ColorPicker(this, opts);
        });
    };
    
})(jQuery,document,window);


function rgbToHex(r, g, b) {
    return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}

function rgbToHsl(r, g, b){
    r /= 255, g /= 255, b /= 255;
    var max = Math.max(r, g, b), min = Math.min(r, g, b);
    var h, s, l = (max + min) / 2;

    if(max == min){
        h = s = 0; // achromatic
        h = s = 0; // achromatic
    }else{
        var d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
        switch(max){
            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
            case g: h = (b - r) / d + 2; break;
            case b: h = (r - g) / d + 4; break;
        }
        h /= 6;
    }
    return [h, s, l];
}

function hexToRgb(hex) {
    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function(m, r, g, b) {
        return r + r + g + g + b + b;
    });

    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}